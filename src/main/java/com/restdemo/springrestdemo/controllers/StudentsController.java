package com.restdemo.springrestdemo.controllers;

import com.restdemo.springrestdemo.models.Student;
import com.restdemo.springrestdemo.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("api/students")
public class StudentsController {

    private StudentService service;

    @Autowired
    public StudentsController(StudentService service) {
        this.service = service;
    }


    @GetMapping
    public List<Student> getStudents() {

        return service.getStudents();

    }

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable int id) {

        return service.getStudentById(id);
    }

    @PostMapping
    public void createStudent(@RequestBody Student student) {

        service.createStudent(student);


    }

    @PutMapping
    public void updateStudent(@PathVariable int id, @RequestBody Student student) {
        service.updateStudent(id, student);
    }

    @DeleteMapping
    public void deleteStudent(@PathVariable int id) {
        service.deleteStudent(id);
    }


}
