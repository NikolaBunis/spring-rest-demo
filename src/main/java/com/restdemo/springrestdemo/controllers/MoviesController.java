package com.restdemo.springrestdemo.controllers;

import com.restdemo.springrestdemo.models.Movie;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/movies")
public class MoviesController {

    List<Movie> movies;

    public MoviesController() {

        this.movies = new ArrayList<>();

        int count = 10;

        for (int i = 0; i < count; i++) {

            Movie movie = new Movie();

            movie.setId(i + 1);
            movie.setTitle("Movie # " + (i + 1));
            movie.setDescription("Description for movie #" + (i + 1));
            movie.setRating(1 + (i % 5));

            movies.add(movie);


        }

    }


    //GET all movies


    //@RequestMapping("/movies")
//@RequestMapping(value  = "/movies", method = RequestMethod.GET)
    @GetMapping
    public List<Movie> getAll() {
        return this.movies;

    }

    //GET movie at ID


    @GetMapping("/{id}")
    public Movie getById(@PathVariable int id) throws ResourceAccessException {


        if (id > movies.size() || id < 0) {

            return null;

        }

        return movies.get(id - 1);


    }

    //POST new movie

    @PostMapping
    //@RequestMapping(value  = "/movies", method = RequestMethod.POST)
    public Movie addMovie(@RequestBody Movie newMovie) {

        newMovie.setId(movies.size() + 1);
        newMovie.setRating(0);


        this.movies.add(newMovie);

        return newMovie;

    }


    //PUT movie at ID

    @PutMapping("/{id}")
    public Movie voteForMovie(@PathVariable int id, @RequestBody int rating) {


        if(rating < 1 || rating > 10){

            return null;

        }

        Movie movie = this.getById(id);

        double oldRatingSum = movie.getRating() * movie.getVotesCount();

        int newVotesCount = movie.getVotesCount() + 1;

        double newRating = (oldRatingSum + rating) / newVotesCount;

        movie.setRating(newRating);
        movie.setVotesCount(newVotesCount);

        return movie;
    }


}
